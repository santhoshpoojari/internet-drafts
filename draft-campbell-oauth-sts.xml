<?xml version="1.0" encoding="us-ascii"?>
<?xml-stylesheet type='text/xsl' href='http://xml2rfc.tools.ietf.org/authoring/rfc2629.xslt' ?>
<!DOCTYPE rfc SYSTEM "rfc2629.dtd">

<?rfc toc="yes"?>
<?rfc tocompact="yes"?>
<?rfc tocdepth="4"?>
<?rfc tocindent="yes"?>
<?rfc symrefs="yes"?>
<?rfc sortrefs="yes"?>
<?rfc comments="yes"?>
<?rfc inline="yes"?>
<?rfc compact="yes"?>
<?rfc subcompact="no"?>

<rfc category="std" docName="draft-campbell-oauth-sts-03" ipr="trust200902">
  <front>
    <title abbrev="an STS for the REST of us">OAuth 2.0 Token Exchange: an STS for the REST of us</title>

    <author fullname="Brian Campbell" initials="B." surname="Campbell">
      <organization>Ping Identity</organization>
      <address><email>brian.d.campbell@gmail.com</email></address>
    </author>

    <author fullname="John Bradley" initials="J." surname="Bradley">
      <organization>Ping Identity</organization>
      <address><email>ve7jtb@ve7jtb.com</email></address>
    </author>

    <date month="July" year="2015" />

    <area>Security</area>
    <workgroup>OAuth Working Group</workgroup>

    <keyword>RFC</keyword>
    <keyword>Request for Comments</keyword>
    <keyword>I-D</keyword>
    <keyword>Internet-Draft</keyword>
    <keyword>Delegation</keyword>
    <keyword>Impersonation</keyword>
    <keyword>STS</keyword>
    <keyword>on-behalf-of</keyword>
    <keyword>act-as</keyword>

    <abstract>
      <t> 
        Much of this document was merged into the WG draft starting at -03 https://tools.ietf.org/html/draft-ietf-oauth-token-exchange-03 
        An OAuth 2.0 framework for exchanging security tokens enabling authorization servers to act as lightweight HTTP and JSON based security token services.
      </t>
    </abstract>

  </front>

  <middle>
    <section anchor="Introduction" title="Introduction">
      <t>
        A security token service (STS) is a service capable of validating and issuing security tokens, which enables web service clients
        to obtain appropriate temporary access credentials for resources in heterogeneous environments or across security domains.
        Clients have historically used
        <xref target="WS-Trust">WS-Trust</xref>
        as the protocol to interact with an STS for token exchange.
        However WS-Trust is a fairly heavyweight framework which uses XML, SOAP, WS-Security, XML-Signatures, etc. while
        the trend in modern web development has been towards more lightweight services utilizing RESTful patterns and JSON.
        <xref target="RFC6749">The OAuth 2.0 Authorization Framework</xref>
        and
        <xref target="RFC6750">OAuth 2.0 Bearer Tokens</xref>
        have emerged as popular standards for authorizing and
        securing access to HTTP and RESTful resources but do not provide all that is needed to support generic STS interactions.
      </t>
      <t>
        This specification defines a lightweight protocol extending OAuth 2.0 that enables clients to request and obtain security tokens from authorization servers acting in the role of an STS. There is support for
        enabling one party to act on behalf of another as well as enabling one party to delegate constrained authority to another.
        Similar to OAuth 2.0, this specification focuses on client developer simplicity and requires only an HTTP client and JSON parser, which are nearly universally available in modern development environments.
        The STS protocol defined in this specification is not itself RESTful (an STS doesn't lend itself particularly well to a REST approach) but does
        utilize communication patterns and data formats that should be more palatable to developers accustom to working with RESTful systems.
      </t>
      <t>
        A new security token request grant type and the associated specific parameters for a security token request to the token endpoint are defined by this specification.
        A security token response is a normal
        OAuth 2.0 response from the token endpoint with some additional parameters defined herein to provide information to the client.
      </t>
      <t>
        The security tokens obtained from an STS could be used in a variety of contexts, the specifics of which are beyond the scope of this document.
      </t>
      <t>
        The scope of this specification is limited to the definition of a framework and basic wire protocol for an STS style token exchange utilizing OAuth 2.0.
        The syntax, semantics and security characteristics of the tokens themselves (both those presented to the AS and those obtained by the client)
        are explicitly out of scope and no requirements are placed on the trust model in
        which an implementation might be deployed. Additional profiles may provide more detailed requirements around the specific nature of the parties and trust involved,
        whether signatures and/or encryption of tokens is required, etc., however, such details will often be policy decisions made with respect
        to the specific needs of individual deployments and will be configured or implemented accordingly.
      </t>
      <section anchor="rnc" title="Requirements Notation and Conventions">
	<t>
	  The key words "MUST", "MUST NOT", "REQUIRED", "SHALL", "SHALL NOT",
	  "SHOULD", "SHOULD NOT", "RECOMMENDED", "NOT RECOMMENDED", "MAY", and "OPTIONAL" in this
	  document are to be interpreted as described in
	  <xref target="RFC2119">RFC 2119</xref>.
	</t>
      </section>

      <section anchor="Terminology" title="Terminology">
        <t>
	  This specification uses the terms "authorization server"
	  "token endpoint", "access token request", "access token response", and "client"
	  defined by <xref target="RFC6749">OAuth 2.0</xref>.
        </t>
      </section>

      <section title="Delegation vs. Impersonation Semantics">
        <t>
          When principal A impersonates principal B, A is given all
          the rights that B has within some defined rights context.
          Whereas, with delegation semantics, principal A still has its own identity
          separate from B and it is explicitly understood that while B
          may have delegated its rights to A, any actions taken are
          being taken by A and not B. In a sense, A is an agent for B.
        </t>
        <t>
          Delegation semantics are therefore different than
          impersonation semantics, with which it is sometimes
          confused. When principal A impersonates principal B, then in
          so far as any entity receiving such a token is concerned, they are
          actually dealing with B. It is true that some members of the
          identity system might have awareness that impersonation is
          going on but it is not a requirement. For all intents and
          purposes, when A is impersonating B, A is B.
        </t>
        <t>
          A security token with delegation semantics is requested using this framework by including
          both an on_behalf_of token and an act_as token in the request. The on_behalf_of token represents the identity of the party on
          behalf of whom the token is being requested while the act_as token represents the identity of the party to whom the access rights of
          the returned token are being delegated.  In this case, the token returned to the client will contain claims about both parties.
        </t>
        <t>
          A security token with impersonation semantics is requested using this framework by including
          an on_behalf_of token in the request and omitting the act_as token. The on_behalf_of token represents the identity of the party on
          behalf of whom the token is being requested the token returned to the client will contain claims about that party.
        </t>
      </section>

    </section>

    <section anchor="TokenRequest" title="Security Token Request">
      <t>
        A client requests a security token by making a token request to the authorization server's token endpoint using the extension grant type mechanism defined
        in <xref target="RFC6749">Section 4.5 of OAuth 2.0</xref>.
      </t>
      <t>
        Client authentication to the authorization server is done using the normal mechanisms provided by OAuth 2.0.
        <xref target="RFC6749">Section 2.3.1 of The OAuth 2.0 Authorization Framework</xref> defines password-based authentication of the client,
        however, client authentication is extensible and other mechanisms are allowed.
        For example, <xref target="RFC7522"></xref> and <xref target="RFC7523"></xref>
        define client authentication using SAML Assertions and JSON Web Tokens respectively. Other mechanisms, such as TLS client authentication, are also possible.
        The supported methods of client authentication and whether or not to allow unauthenticated or unidentified clients are deployment decisions that are at the discretion of the authorization server.
      </t>

      <t>The client makes a general security token request to the token endpoint with an extension
        grant type by including the
        following parameters using the <spanx style='verb'>application/x-www-form-urlencoded</spanx>
        format with a character encoding of UTF-8 in the HTTP request entity-body:
      </t>
      <t><list style="hanging">

        <t hangText="grant_type">
          <vspace/>
          REQUIRED. The value <spanx style='verb'>urn:ietf:params:oauth:grant-type:security-token-request</spanx>
          indicates that it is a security token request.</t>

        <t hangText="aud">
          <vspace/>
          OPTIONAL.
          Indicates the location of the service or resource where the client intends to use the requested security token.
          The value MUST be an absolute URI as defined by Section 4.3 of <xref target="RFC3986"/>. The URI MAY include a
          query component but MUST NOT include a fragment component. When applicable, the value of this parameter also typically informs the
          audience restrictions on the returned security token.</t>

        <t hangText="scope">
          <vspace/>
          OPTIONAL.
          A list of space-delimited, case-sensitive strings that allow the client to specify the desired scope of requested
          security token in the context of the service or resource where the token will be used (possibly indicated by the <spanx style='verb'>aud</spanx> parameter).</t>

        <t hangText="requested_security_token_type">
          <vspace/>
          OPTIONAL.
          Identifier for the type of the requested security token. For example, a JWT can be requested with the identifier
          <spanx style="verb">urn:ietf:params:oauth:token-type:jwt</spanx>,
          which is defined in <xref target="RFC7519">JSON Web Token</xref>.
          If the requested type is unspecified, the returned token type is at the discretion of the
          authorization server and may be dictated by knowledge of the requirements of the service or resource whose location is
          indicated by the <spanx style='verb'>aud</spanx> parameter.
        </t>

        <t hangText="on_behalf_of">
          <vspace/>
          REQUIRED.
          The value of this request parameter is a security token which represents the identity of the party on behalf of whom the request is being made.
          Typically the subject of this token will be the primary subject of the security token returned in response to this request.
        </t>

        <t hangText="on_behalf_of_token_type">
          <vspace/>
          REQUIRED.
          An identifier that indicates the type of the security token sent with the <spanx style="verb">on_behalf_of</spanx> parameter.
        </t>

        <t hangText="act_as">
          <vspace/>
          OPTIONAL.
          The value of this request parameter is a security token which represents the identity of the party that is authorized to use the requested security token.
          When this parameter is present, it indicates that the client wants a token that contains claims about two distinct entities:
          1) the entity represented by the token in the <spanx style="verb">on_behalf_of</spanx> parameter as the primary subject and 2)
          the entity represented by this token as a party who is authorized to act on behalf of that subject.
        </t>

        <t hangText="act_as_token_type">
          <vspace/>
          REQUIRED when the <spanx style="verb">act_as</spanx> parameter is present in the request but MUST NOT be included otherwise.
            The value of this parameter is
          an identifier that indicates the type of the security token sent with the <spanx style="verb">act_as</spanx> parameter.
        </t>
      </list></t>

    </section>
    <section anchor="TokenResponse" title="Security Token Response">
      <t>
        The authorization server responds to a security token request with a normal OAuth 2.0 response from the token endpoint as defined in
        <xref target="RFC6749">Section 5 of RFC 6749</xref>. Additional details and explanation are provided in the following subsections.
      </t>
        <section anchor="SuccessfulResponse" title="Successful Security Token Response">
          <t>
            If the request is valid and meets all policy and other criteria of the authorization server,
            a successful token response is constructed by adding the following parameters
            to the entity-body of the HTTP response using the "application/json"
            media type as defined by <xref target="RFC4627"/> and an HTTP 200 status code.  The
            parameters are serialized into a JavaScript Object Notation (JSON)
            structure by adding each parameter at the top level.
            Parameter names and string values are included as JSON strings.
            Numerical values are included as JSON numbers.  The order of
            parameters does not matter and can vary.
          </t>
          <t><list style="hanging">

          <t hangText="access_token">
            <vspace/>
            REQUIRED. The security token issued by the authorization server in response to the security token request.
            The <spanx style="verb">access_token</spanx> parameter from <xref target="RFC6749">Section 5.1 of RFC 6749</xref> is used here
            to carry the requested security token, which allows this token exchange framework to use the existing OAuth 2.0 request
            and response constructs defined for the token endpoint.
          </t>

          <t hangText="security_token_type">
            <vspace/>
            REQUIRED.  An identifier for the general type of the returned security token.
            For example, if the security token is a JWT, this value of the <spanx style="verb">security_token_type</spanx> is
            <spanx style="verb">urn:ietf:params:oauth:token-type:jwt</spanx>.
          </t>

          <t hangText="token_type">
              <vspace/>
            REQUIRED.  A case insensitive value describing the type of the token issued as discussed in
             <xref target="RFC6749">Section 7.1 of RFC 6749</xref>. Note that this value is different from
            the value of the <spanx style="verb">security_token_type</spanx> and provides the client with information
            about how to utilize the token to access protected resources. For example, a value of <spanx style="verb">Bearer</spanx> as defined
            in <xref target="RFC6750"/> indicates that the security token is a bearer token and the client can simply
            present it as is without any additional proof of eligibility beyond the contents of the token itself.
            A value of <spanx style="verb">PoP</spanx>, on the other hand, indicates that use of the token will require demonstrating possession of a
            cryptographic key associated with the security token (<xref target="I-D.ietf-oauth-pop-key-distribution"/> describes the <spanx style="verb">PoP</spanx>
            token type).
          </t>

          <t hangText="expires_in">
            <vspace/>
            RECOMMENDED.  The validity lifetime, in seconds, of the issued security token.  For
            example, the value 3600 denotes that the token will
            expire in one hour from the time the response was generated.
          </t>

          <t hangText="scope">
            <vspace/>
            OPTIONAL, if the scope of the security token is identical to the scope requested by the client;
            otherwise, REQUIRED.</t>

          <t hangText="refresh_token">
              <vspace/>
            NOT RECOMMENDED.
            Refresh tokens will typically not be issued in response to a <spanx style="verb">urn:ietf:params:oauth:grant-type:security-token-request</spanx> grant type requests.
          </t>

          </list></t>
        </section>
        <section anchor="ErrorResponse" title="Error Response">
          <t>If either the <spanx style="verb">on_behalf_of</spanx> or <spanx style="verb">act_as</spanx>
            tokens are invalid for any reason, or are unacceptable based on policy, the authorization server
            MUST construct an error response as defined in <xref target="RFC6749">Section 5.2 of OAuth 2.0</xref>
            The value of the <spanx style='verb'>error</spanx>
            parameter MUST be the <spanx style='verb'>invalid_grant</spanx> error code. The authorization
            server MAY include additional information regarding the reasons for the error using the <spanx style='verb'>error_description</spanx> or
            <spanx style='verb'>error_uri</spanx> parameters.</t>
        </section>
    </section>
    <section anchor="Examples" title="Examples">
      <t>[[ TODO:  at least two examples, with and without act_as, showing a request/response exchange and including some relevant internal details of the tokens involved ]] </t>
    </section>
    <section anchor="IANA" title="IANA Considerations">
      <t>[[ TODO ]]
        The <spanx style="verb">urn:ietf:params:oauth:grant-type:security-token-request</spanx>
	Grant Type is to be registered in the
	IANA urn:ietf:params:oauth registry established in <xref target="RFC6755"/>.
      </t>
      <t>[[ TODO ]]
        Other parameters like <spanx style="verb">requested_security_token_type</spanx>,
        <spanx style="verb">on_behalf_of</spanx>, <spanx style="verb">on_behalf_of_token_type</spanx>, <spanx style="verb">act_as</spanx>,
        etc. need to be registered in the appropriate registries.
        The <spanx style="verb">aud</spanx> parameter needs to be registered too but that may well get done in <xref target="I-D.ietf-oauth-pop-key-distribution"/> and
        aud may/does have wider applicability so perhaps deserves it's own little spec?
      </t>

    </section>

    <section anchor="Security" title="Security Considerations">
      <t>
        [[ TODO ]]
      </t>
    </section>
  </middle>

  <back>
    <references title="Normative References">
      <?rfc include="reference.RFC.2119"?>
      <?rfc include="reference.RFC.6749"?>
      <?rfc include="reference.RFC.3986"?>
      <?rfc include="reference.RFC.4627"?>
    </references>

    <references title="Informative References">
      <?rfc include="reference.RFC.6755"?>
      <?rfc include="reference.RFC.6750"?>
      <?rfc include="reference.RFC.7522"?>
      <?rfc include="reference.RFC.7523"?>
      <?rfc include="reference.I-D.jones-oauth-token-exchange"?>
      <?rfc include="reference.RFC.7519"?>
      <?rfc include="reference.I-D.ietf-oauth-pop-key-distribution"?>

      <reference anchor="WS-Trust" target="http://docs.oasis-open.org/ws-sx/ws-trust/v1.4/ws-trust.html">
      	<front>
      	  <title>WS-Trust 1.4 (incorporating Approved Errata 01)</title>
      	  <author fullname="Anthony Nadalin" initials="A." surname="Nadalin"/>
      	  <author fullname="Marc Goodner" initials="M." surname="Goodner"/>
      	  <author fullname="Martin Gudgin" initials="M." surname="Gudgin"/>
      	  <author fullname="Abbie Barbir" initials="A." surname="Barbir"/>
      	  <author fullname="Hans Granqvist" initials="H." surname="Granqvist"/>
      	  <date day="2" month="February" year="2012"/>
      	</front>
      </reference>

      <reference anchor="OpenID.Core">
        <front>
          <title>OpenID Connect Core 1.0</title>
          <author fullname="Nat Sakimura" initials="N." surname="Sakimura"/>
          <author fullname="John Bradley" initials="J." surname="Bradley"/>
          <author fullname="Michael B. Jones" initials="M.B." surname="Jones"/>
          <author fullname="Breno de Medeiros" initials="B." surname="Medeiros"/>
          <author fullname="Chuck Mortimore" initials="C." surname="Mortimore"/>
          <date day="25" month="February" year="2014"/>
        </front>
        <format target="openid.net/specs/openid-connect-core-1_0.html" type="HTML" />
      </reference>

    </references>

   <section title="Open Issues" anchor="open.issues">
      <t>
        Some, but surely not all, decisions to be made with potential associated draft updates:
      	<list style="symbols">
          <t>
            Are the constructs for expressing delegation and impersonation the 'right' ones? Do they provide sufficient flexibility while
            being reasonably understandable and implementable?
          </t>
          <t>Does it really make sense to use the act_as and on_behalf_of terms? They come with some baggage.</t>
          <t>
            More guidance on what delegation should look like in the returned token? I.e. refer to <spanx style="verb">azp</spanx> in <xref target="OpenID.Core"/>? Or something else?
          </t>
          <t>
            Do we need to codify if/how the identity of the client end up in returned token? Should it be an AS decision? A special case of delegation/act_as? Something else?
          </t>
          <t>
            Does the error response need a way to convey additional information beyond what OAuth 2.0 provides?
          </t>
          <t>
            Exactly how the presentation of PoP or other non-bearer tokens works. Should a challenge-response mechanism be considered rather than trying to stuff the whole PoP into a single request?
          </t>
        </list>
      </t>
    </section>

    <section title='Acknowledgements' anchor='acks'>
      <t>
        The author wishes to thank Michael Jones for bringing forth the concept
        of OAuth 2.0 Token Exchange with <xref
        target="I-D.jones-oauth-token-exchange"/>. This draft borrows heavily
        from Jones' work while striving to provide a syntax that is more
        consistent with <xref target="RFC6749">OAuth 2.0</xref>, which will hopefully be
        more familiar to developers and easier to understand and implement.
       </t><t>
        The author also wishes to thank John Bradley for his endless patience and willingness to share his expertise.
      </t>
      <t>
        The following individuals also contributed ideas, feedback, and wording that shaped and formed the final specification:
      </t>
      <t>
        Chuck Mortimore, Justin Richter, Phil Hunt, and Scott Tomilson.
      </t>
    </section>

    <section title="Document History" anchor="History">
      <t>
	[[ to be removed by the RFC Editor before publication as an RFC ]]
      </t>
      <t>
        -03
        <list style='symbols'>
          <t>
            The aud parameter is now OPTIONAL rather than REQUIRED.
          </t>
          <t>
            Update references for JWT and SAML/JWT assertion frameworks to the new RFCs.
          </t>
        </list>
      </t>
      <t>
        -02
        <list style='symbols'>
          <t>
            Refreshing draft before -01 expires.
          </t>
        </list>
      </t>
      <t>
        -01
        <list style='symbols'>
          <t>
            Add Bradley as an author.
          </t>
        </list>
      </t>
      <t>
        -00
        <list style='symbols'>
          <t>
            Gotta start somewhere...
          </t>
        </list>
      </t>
    </section>

  </back>
</rfc>
